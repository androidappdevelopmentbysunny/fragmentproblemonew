package co.megaminds.fragmentprobleminew.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import co.megaminds.fragmentprobleminew.R;


public class FirstFragment extends Fragment {

    private View rootView;



    private Fragment fragment;
    private FragmentManager fragmentManager;
    private  FragmentTransaction fragmentTransaction;
private String answerTitle;
    public FirstFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragment = new NewFragment();
        fragmentManager = getActivity().getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();

    }




    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {



        rootView = inflater.inflate(R.layout.fragment_first, container, false);

        Button updateButton = (Button) rootView.findViewById(R.id.bt_for_go_new_fragment);
        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {




                fragmentTransaction.replace(R.id.my_container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();



            }
        });





    return rootView;
}





}
