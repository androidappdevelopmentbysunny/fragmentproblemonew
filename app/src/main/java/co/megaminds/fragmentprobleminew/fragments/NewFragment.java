package co.megaminds.fragmentprobleminew.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import co.megaminds.fragmentprobleminew.R;


public class NewFragment extends Fragment  {

    private Context context;
    private View rootView;
    long ePID = 0;

    public NewFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {

        super.onResume();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_new, container, false);
        context = getActivity();


        return rootView;

    }



}
